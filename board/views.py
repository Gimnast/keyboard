from django.shortcuts import render
from .models import *


def index(request):
    context = {
        "current_line": "",
        "language": "English",
        "caps": True,
        "status": "Тут пусто",
    }
    if request.method == 'POST':
        args = request.POST
        if "status" in args:
            context["status"] = args["status"]
        if "current_caps" in args:
            if args["current_caps"] == "True":
                context["caps"] = True
            else:
                context["caps"] = False
        if "current_language" in args:
            context["language"] = args["current_language"]
        if "caps" in args:
            context["caps"] = not context["caps"]
        if "language" in args:
            if context["language"] == "English":
                context["language"] = "Russian"
                context["current_line"] = ""
            else:
                context["language"] = "English"
                context["current_line"] = ""
            return render(request, 'index.html', context)
        if "line" in args:
            context["current_line"] = args["line"]
        if "pressed" in args:
            if args["pressed"] == "delete":
                context["current_line"] = context["current_line"][:-1]
            elif args["pressed"] == "clear":
                context["current_line"] = ""
            elif len(context["current_line"]) < 25 and\
                    args["pressed"] != "delete":
                context["current_line"] += args["pressed"]
        if "view_code" in args:
            code = ""
            for i in context["current_line"]:
                try:
                    decode_symbol = Unicode.objects.get(symbol=i)
                    code += str(decode_symbol.id)
                except:
                    return render(request, 'index.html', context)
                context["status"] = code
        if "view_letter" in args:
            letter = ""
            try:
                code = int(context["current_line"])
            except:
                context["status"] = "Введите корректные данные"
                return render(request, 'index.html', context)
            code_parsed = []
            while code > 0:
                code_parsed.append(code % 10)
                code //= 10
            code_parsed = list(reversed(code_parsed))
            expected_code = 0
            error_schet = 0
            for i in code_parsed:
                expected_code *= 10
                expected_code += i
                if context["language"] == "Russian" and\
                        (expected_code > 1000 or expected_code <= 57):
                    try:
                        some = Unicode.objects.get(id=expected_code)
                        letter += some.symbol
                        expected_code = 0
                        error_schet = 0
                    except:
                        error_schet = 1
                elif context["language"] == "English" and\
                        (expected_code < 1000):
                    try:
                        some = Unicode.objects.get(id=expected_code)
                        letter += some.symbol
                        expected_code = 0
                        error_schet = 0
                    except:
                        error_schet = 1
            if error_schet:
                context["status"] = "Введите корректные данные"
                return render(request, 'index.html', context)
            context["status"] = letter
            return render(request, 'index.html', context)
    return render(request, 'index.html', context)
