from django.db import models


class Unicode(models.Model):
    symbol = models.CharField(max_length=25)
